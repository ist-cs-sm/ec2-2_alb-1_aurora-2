resource "aws_route53_zone" "site_zone" {
  name = "${var.root_domain}"
}

resource "aws_route53_record" "site" {
  zone_id = "${aws_route53_zone.site_zone.zone_id}"
  name = "${var.site_domain}"
  type = "A"

  alias {
    name = "${aws_alb.alb.dns_name}"
    zone_id = "${aws_alb.alb.zone_id}"
    evaluate_target_health = false
  }
}