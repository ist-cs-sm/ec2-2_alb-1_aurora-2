## Public Route table

resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    Name = "Route-public-${var.site_domain}"
  }
}

resource "aws_route" "public" {
  destination_cidr_block = "0.0.0.0/0"
  route_table_id         = "${aws_route_table.public.id}"
  gateway_id             = "${aws_internet_gateway.main.id}"
}

resource "aws_route_table" "private" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    Name = "Route-private-${var.site_domain}"
  }
}

resource "aws_route_table_association" "public-1a" {
  subnet_id = aws_subnet.public_subnet_1a.id
  route_table_id = "${aws_route_table.public.id}"
}

resource "aws_route_table_association" "public-1c" {
  subnet_id = aws_subnet.public_subnet_1c.id
  route_table_id = "${aws_route_table.public.id}"
}


resource "aws_route_table_association" "private-1a" {
  subnet_id = aws_subnet.private_subnet_1a.id
  route_table_id = "${aws_route_table.private.id}"
}

resource "aws_route_table_association" "private-1c" {
  subnet_id = aws_subnet.private_subnet_1c.id
  route_table_id = "${aws_route_table.private.id}"
}