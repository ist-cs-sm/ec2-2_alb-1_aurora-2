#!/bin/bash
# for userdata

# general setting

setenforce 0
sed -i "s/SELINUX\=enforcing/SELINUX\=disabled/g" /etc/selinux/config 
timedatectl set-timezone Asia/Tokyo
hostnamectl set-hostname WEB2

dd if=/dev/zero of=/swapfile1 bs=1M count=1024
chmod 600 /swapfile1
mkswap /swapfile1
swapon /swapfile1
sh -c 'echo "/swapfile1  none        swap    sw              0   0" >> /etc/fstab'

cat << EOS >> /etc/sysctl.conf
net.ipv4.conf.all.secure_redirects = 1
net.ipv4.conf.all.accept_redirects = 0
net.ipv6.conf.all.accept_redirects = 0
net.ipv4.conf.all.send_redirects = 0
net.ipv4.conf.all.accept_source_route = 0
net.ipv6.conf.all.accept_source_route = 0
net.ipv4.conf.all.log_martians = 1
EOS

sed -i "s/BindIPv6Only/#BindIPv6Only/g" /usr/lib/systemd/system/rpcbind.socket
sed -i "s/ListenStream/#ListenStream/g" /usr/lib/systemd/system/rpcbind.socket
sed -i "s/ListenDatagram/#ListenDatagram/g" /usr/lib/systemd/system/rpcbind.socket
sed -i "s/ListenStream/#ListenStream/g" /usr/lib/systemd/system/rpcbind.socket
sed -i "s/ListenDatagram/#ListenDatagram/g" /usr/lib/systemd/system/rpcbind.socket
systemctl daemon-reload

echo "HISTTIMEFORMAT='%F %T '" >> /etc/profile
sed -i 's/HISTSIZE=1000/HISTSIZE=10000000/' /etc/profile
source /etc/profile

echo 'PS1="\[\e[1;34m\][\u@\h \W]\\$ \[\e[m\]"' >> ~/.bashrc
source ~/.bashrc

seep 5

yum -y install lsof vim-e* sysstat epel-release unzip httpd httpd-devel openssl

sleep 10

# Apache install
mkdir -p /etc/httpd/conf.d/virtual/
mkdir -p /var/log/httpd/CNAME/
mkdir -p /var/www/CNAME/istadmin
mkdir -p /var/www/CNAME/logs
mkdir -p /var/www/CNAME/htdocs
mkdir -p /var/www/CNAME/config

chown -R apache. /var/www/
find /var/www/ -type d -exec chmod 2775 {} +
find /var/www/ -type f -exec chmod 665 {} +

sed -i "s/^/#/g" /etc/httpd/conf.d/welcome.conf
sed -i "s/^/#/g" /etc/httpd/conf.d/autoindex.conf
sed -i "s/^/#/g" /etc/httpd/conf.d/userdir.conf 

sed -i "s/Indexes//g" /etc/httpd/conf/httpd.conf
sed -i "s/Indexes//g" /etc/httpd/conf.d/*

cat << EOF > /etc/httpd/conf.d/security.conf
        ServerTokens Prod
        Header always unset X-Powered-By     
        RequestHeader unset Proxy
        TraceEnable Off
        Header always set X-XSS-Protection "1; mode=block"
        Header always set X-Frame-Options "SAMEORIGIN"
        Header always set X-Content-Security-Policy "default-src 'self'"
        Header always set X-Content-Type-Options nosniff
        Header always set Pragma no-cache
        Header always set Cache-Control no-store
EOF

cat   << EOF > /var/www/CNAME/htdocs/index.html

<html>
	<head>
		<title>CNAME</title>
	</head>

	<body>
		$CNAME <br>
	    `date "+%Y%m%d-%H%M%S"`
	</body>
</html>

EOF


cat   << EOF > /etc/httpd/conf.d/virtual/02-CNAME.conf
<VirtualHost *:80>
        ServerAdmin system
        ServerName CNAME
        DirectoryIndex index.html index.php

        ErrorLog /var/www/CNAME/logs/error_log
        CustomLog /var/www/CNAME/logs/access_log combined

        DocumentRoot /var/www/CNAME/htdocs
        DirectoryIndex index.html index.php

        RemoteIPHeader X-Forwarded-For

         <IfModule mod_rewrite.c>
                RewriteEngine on
                RewriteCond %{REQUEST_METHOD} ^TRACE [OR]
                RewriteCond %{REQUEST_METHOD} ^SEARCH [OR]
                RewriteCond %{REQUEST_METHOD} ^OPTIONS
                RewriteRule .* - [F]
        </IfModule>

        <Directory "mkdir -p /var/www/CNAME/htdocs">
    Options FollowSymLinks
    AllowOverride AuthConfig FileInfo Limit
 #   <RequireAll>
 #     AuthType basic
 #     AuthName "$CNAME"
 #     AuthUserFile /etc/httpd/conf/.htpasswd
 #     AuthGroupFile /dev/null
 #     require valid-user
 #   </RequireAll>
        </Directory>
        
        
#  <IfModule mod_php7.c>
#    php_value include_path .:/usr/local/lib/php:/var/www/$CNAME/config:/usr/share/pear
# </IfModule>

  Alias /istadmin /var/www/CNAME/istadmin
  <Directory "/var/www/CNAME/istadmin">
    Options FollowSymLinks
    AllowOverride AuthConfig FileInfo Limit    
#    <RequireAll>
#      AuthType basic
#      AuthName "CNAME"
#      AuthUserFile /etc/httpd/conf/.htpasswd
#      AuthGroupFile /dev/null
#      require user takeda-kenko
      <RequireAny>
        # ist backup
        Require ip 60.32.5.238
        Require ip 60.32.5.234
        # ist main
        Require ip 124.35.96.10
        Require ip 124.35.96.13
        # ist VPN
        Require ip 202.248.28.196
      </RequireAny>
#    </RequireAll>
  </Directory>
</VirtualHost>
EOF

cat   << EOF > /etc/httpd/conf.d/virtual/01-default.conf
        <VirtualHost *:80> 
        ServerName any 
        ErrorLog /var/log/html/error_log
        CustomLog /var/log/html/access_log combined
        
        RemoteIPHeader X-Forwarded-For
        
        <Location />
          Require all denied
		      Require ip 127.0.0.1
		      Require ip 10.0.0.0
    		</Location>
        </VirtualHost> 
EOF

systemctl enable httpd
systemctl start httpd

cat   << EOF > /etc/logrotate.d/httpd 
/var/log/httpd/*log 
/var/www/$CNAME/logs/*log 
{
    ifempty
    compress
    daily
    rotate 90
    missingok
    notifempty
    sharedscripts
    delaycompress
    postrotate
        /bin/systemctl reload httpd.service > /dev/null 2>/dev/null || true
    endscript
    dateext
    dateformat .%Y-%m-%d 
}

EOF


yum -y update
sync