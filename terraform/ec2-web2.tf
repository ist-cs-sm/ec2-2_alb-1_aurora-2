resource "aws_eip" "web2" {
  instance = aws_instance.web2.id
  vpc      = true
}

resource "aws_instance" "web2" {
    ami = data.aws_ami.centos7_ami.id
    instance_type = "${var.instancetype}"
    key_name = aws_key_pair.key_pair.key_name


    vpc_security_group_ids = [
      "${aws_security_group.Sg-localaccess.id}",
      "${aws_security_group.Sg-IST-SSH.id}",
      "${aws_security_group.Sg-JIGSAW-SSH.id}",
      "${aws_security_group.Sg-IST-ICMP.id}",
      "${aws_security_group.Sg-JIGSAW-ICMP.id}"
    ]
    subnet_id = "${aws_subnet.public_subnet_1a.id}"
    associate_public_ip_address = "false"
    ebs_block_device {
      device_name    = "/dev/sda1"
      volume_type = "gp2"
      volume_size = "${var.EBSsize}"
      }
    user_data   = "${file("./ec2-userdata-web2.sh")}"
    tags  = {
        Name = "web2"
    }
}
