## ALB

resource "aws_alb" "alb" {
  name                       = "ALB-${var.projectname}"
  security_groups            = ["${aws_security_group.Sg-alb.id}"]
  subnets                    = ["${aws_subnet.public_subnet_1a.id}","${aws_subnet.public_subnet_1c.id}"]
  internal                   = false
  enable_deletion_protection = false

}


## target group 
resource "aws_alb_target_group" "alb" {
  name     = "TargetG-${var.projectname}"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${aws_vpc.vpc.id}"

  health_check {
    interval            = 30
    path                = "/index.html"
    port                = 80
    protocol            = "HTTP"
    timeout             = 5
    unhealthy_threshold = 2
    matcher             = 200
  }
}

resource "aws_alb_target_group_attachment" "web1" {
  target_group_arn = "${aws_alb_target_group.alb.arn}"
  target_id        = "${aws_instance.web1.id}"
  port             = 80
}

resource "aws_alb_target_group_attachment" "web2" {
  target_group_arn = "${aws_alb_target_group.alb.arn}"
  target_id        = "${aws_instance.web2.id}"
  port             = 80
}


## listener


resource "aws_alb_listener" "alb" {
  load_balancer_arn = "${aws_alb.alb.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "Forbidden"
      status_code  = "403"
    }
}
}
resource "aws_alb_listener_rule" "origin" {
  listener_arn = "${aws_alb_listener.alb.arn}"
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.alb.arn
  }

  condition {
    host_header {
      values = ["${var.site_domain}"]
    }
  }
}

#resource "aws_alb_listener" "alb" {
#  load_balancer_arn = "${aws_alb.alb.arn}"
#  port              = "443"
#  protocol          = "HTTPS"
#  ssl_policy        = "TLS-1-2-Ext-2018-06"
#  certificate_arn   = "${var.alb_config["certificate_arn"]}"
#
#  default_action {
#    type = "fixed-response"
#
#    fixed_response {
#      content_type = "text/plain"
#      message_body = "Forbidden"
#      status_code  = "403"
#    }
#}

#resource "aws_lb_listener_rule" "static" {
#  listener_arn = aws_lb_listener.alb.arn
#  priority     = 100
#
#  action {
#    type             = "forward"
#    target_group_arn = aws_lb_target_group.alb.arn
#  }
#
#  condition {
#    host_header {
#      values = ["${var.site_domain}"]
#    }
#  }
#}
