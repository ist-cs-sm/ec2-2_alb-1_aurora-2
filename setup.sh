#!/bin/bash

echo "Please type project name.(ex.hogehoge)"

read PROJECT

echo "Please type Website's doomain name.(ex.www.hogehoge.com)"

read CNAME

echo "Please type root domain name (ex.hogehoge.com)"

read ROOTDOMAIN

echo "Please type your AWS profile name"

read AWSPROFILE

echo "Please type your stage's name (ex, live/staging/preview)"

read STAGE

echo "Please type Instance type of Web servers (ex, t3.small)"

read INSTANCETYPE

echo "Please type EBSvolumesize of Web servers (ex, 50)"

read EBSSIZE

echo "Please type Aurora's username"

read DBUSER

echo "Please type Aurora's password"

read DBPASS

echo "Please type Instance type of Aurora (ex, db.t3.small)"

read DBINSTANCETYPE

sed -i "s/defaultprofile/$AWSPROFILE/g" terraform/main.tf 

sed -i "s/defaultprofile/$AWSPROFILE/g" terraform/acm.tf

sed -i -e "s/CNAME/$CNAME/g" terraform/ec2-userdata-web1.sh  
sed -i -e "s/CNAME/$CNAME/g" terraform/ec2-userdata-web2.sh  

cat  << EOF > terraform/variables.tf

# ---------------
# variables.tf
# ---------------


variable "projectname" {
  default = "$PROJECT"
}

variable "site_domain" {
  default = "$CNAME"
}

variable "root_domain" {
  default = "$ROOTDOMAIN"
}

variable "stage" {
  default = "$STAGE"
}

variable "instancetype" {
  default = "$INSTANCETYPE"
}

variable "EBSsize" {
  default = "$EBSSIZE"
}

variable "dbusername" {
  default = "$DBUSER"
}

variable "dbpassword" {
  default = "$DBPASS"
}

variable "cluster_instance_class" {
  default = "$DBINSTANCETYPE"
}

variable "key_name" {
  default = "$PROJECT"
}


EOF



cd terraform ; terraform init
HOSTZONEID=`aws route53 list-hosted-zones --profile $AWSPROFILE --output text | grep $ROOTDOMAIN | awk -F '/' '{print $3}' | awk '{print $1}'`
terraform import aws_route53_zone.site_zone $HOSTZONEID
terraform plan

echo "Continue? (yes/no)"
while true ; do
    read ANSWER
    case ${ANSWER} in
        yes)
            break
            ;;
         no)
            echo "Terminated."
            exit 0
            ;;
          *)
            echo "Characters other than "yes" or "no" has been entered. Please re-enter."
            ;;
    esac
done

terraform apply

